import React from 'react';
import PropTypes from 'prop-types';

import Date from './Date';

class DatesPanel extends React.Component {

    _displayDays = () => {
        let cells = [];
        let rows = [];
        this.props.days.forEach((day, i) => {
            if (i % 7 !== 0 || i === 0) {
                cells.push(<Date key={day.id} name={day.value} currentMonth={day.currentMonth} active={day.active} appointment={day.appointment}/>);
            } else {
                rows.push(cells);
                cells = [];
                cells.push(<Date key={day.id} name={day.value} currentMonth={day.currentMonth} active={day.active} appointment={day.appointment}/>);
            }
            if (i === this.props.days.length - 1) {
                rows.push(cells);
            }
        });
        return rows;
    };

    render() {
        return (
            <table>
                <thead>
                <tr>
                    {
                        this.props.weekDays.map((day, key) => {
                            return <Date key={key} name={day} currentMonth/>
                        })
                    }
                </tr>
                </thead>
                <tbody>
                {this._displayDays().map((cell, key) => {
                    return <tr key={key}>{cell}</tr>
                })}
                </tbody>
                <tfoot>

                </tfoot>
            </table>
        )
    }
}

DatesPanel.propTypes = {
    weekDays: PropTypes.array.isRequired,
    days: PropTypes.array.isRequired,
};

export default DatesPanel;
