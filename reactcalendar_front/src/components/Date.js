import React from 'react';
import PropTypes from 'prop-types';

import { Button } from 'shards-react';

import {css, StyleSheet} from 'aphrodite';

class Date extends React.Component {

    render() {
        return (
            <td className={css(styles.container)}>
                <Button
                    className={css(styles.button, this.props.currentMonth ? styles.currentMonth : "", this.props.active ? styles.active : "")}
                    squared>
                    {this.props.name}
                    {this.props.appointment ? <div className={css(styles.circle)}></div> : ""}
                </Button>
            </td>
        )
    }
}


Date.propTypes = {
    name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    currentMonth: PropTypes.bool,
    active: PropTypes.bool,
    appointment: PropTypes.bool,
};

const styles = StyleSheet.create({
    container: {
        padding: 10
    },
    button: {
        borderStyle: 'none',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'white',
        backgroundColor: '#2980b9',
        borderColor: '#2980b9',
        fontSize: '2em',
        width: 70,
        height: 70,
        opacity: 0.5,
        ':focus': {},
        ':hover': {
            opacity: 0.5
        }
    },
    currentMonth: {
        opacity: 1,
    },
    active: {
        backgroundColor: 'white',
        borderColor: '#2980b9',
        color: "#2980b9",
    },
    circle: {
        marginTop: 30,
        backgroundColor: '#44bd32',
        borderRadius: '50%',
        width: 8,
        height: 8,
        position: 'absolute',
    }
});

export default Date;
