import React from 'react';
import PropTypes from 'prop-types';

import {css, StyleSheet} from 'aphrodite';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft, faChevronRight} from '@fortawesome/free-solid-svg-icons';

class Header extends React.Component {

    _onClick = (value) => {
        this.props.onClick(value);
    };

    render() {
        return (
            <div className={css(styles.container)}>
                <div>
                    <button className={css(styles.button)} onClick={() => this._onClick(false)}>
                        <FontAwesomeIcon icon={faChevronLeft}
                                         size={"3x"}
                                         color={"#bdc3c7"}/>
                    </button>
                </div>
                <div>{this.props.date.format("MMMM YYYY")}</div>
                <div>
                    <button className={css(styles.button)} onClick={() => this._onClick(true)}>
                        <FontAwesomeIcon
                            icon={faChevronRight}
                            size={"3x"}
                            color={"#bdc3c7"}/>
                    </button>
                </div>
            </div>
        )
    }
}


Header.propTypes = {
    date: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
    container: {
        fontSize: '2em',
        color: '#bdc3c7',
        display: 'flex',
        justifyContent: 'space-between',
        padding: 10,
    },
    button: {
        borderStyle: 'none',
    }
});

export default Header;
